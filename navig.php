<nav>
	<section class="nav">
		<img src="" alt="" class="logo">
		<nav>
			<a href="#" class="toggle-button">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</a>
			<ul>
				<li>
					<?php echo "<a href='index.php?page=home&css=".$css."&titre=introduction'>Accueil</a>"; ?>
				</li>
				<li class="sub-menu"><a href="#">CSS3</a>
					<ul>
						<li>
							<?php echo "<a href='index.php?page=transformation&css=".$css."&titre=Transformations'>Les transformations</a>"; ?>
						</li>
						<li>
							<?php echo "<a href='index.php?page=animation&css=".$css."&titre=Animations'>Les animations</a>"; ?>
						</li>
						<li>
							<?php echo "<a href='index.php?page=background&css=".$css."&titre=Background'>Les background-images</a>"; ?>

						</li>
					</ul>
				</li>
				<li>
					<?php echo "<a href='index.php?page=liens&css=".$css."&titre=Liens'>Les Liens utiles</a>"; ?>
				</li>
			</ul>
		</nav>
	</section>

</nav>

