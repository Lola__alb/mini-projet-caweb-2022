/*Animation pour la page css animations*/
let reset = document.querySelector("#reset");
let anime1 = document.querySelector("#anime1");
let anime2 = document.querySelector("#anime2");
let anime3 = document.querySelector("#anime3");
let anime4 = document.querySelector("#anime4");
let code0 = document.querySelector(".code0");
let code1 = document.querySelector(".code1");
let code2 = document.querySelector(".code2");
let code3 = document.querySelector(".code3");
let code4 = document.querySelector(".code4");
let duckAnim = document.querySelector(".animated-example");

reset.addEventListener("click", duckReset);
anime1.addEventListener("click", duckBounce);
anime2.addEventListener("click", duckJump);
anime3.addEventListener("click", duckFloat);
anime4.addEventListener("click", duckAppear);

function duckReset(){
    hideCodes();
    duckAnim.classList.remove("duckBounce", "duckJump", "duckFloat", "duckAppear");
    code0.classList.remove("hide");

}
function duckBounce(){
 hideCodes();
 duckAnim.classList.remove("duckBounce", "duckJump", "duckFloat", "duckAppear");
 duckAnim.classList.add("duckBounce");
 code1.classList.remove("hide");
}
function duckJump(){
    hideCodes();
    duckAnim.classList.remove("duckBounce", "duckJump", "duckFloat", "duckAppear");
    duckAnim.classList.add("duckJump");
    code2.classList.remove("hide");
}
function duckFloat(){
    hideCodes();
    duckAnim.classList.remove("duckBounce", "duckJump", "duckFloat", "duckAppear");
    duckAnim.classList.add("duckFloat");
    code3.classList.remove("hide");
}
function duckAppear(){
    hideCodes();
    duckAnim.classList.remove("duckBounce", "duckJump", "duckFloat", "duckAppear");
    duckAnim.classList.add("duckAppear");
    code4.classList.remove("hide");
}

function hideCodes(){
   code0.classList.add("hide");
   code1.classList.add("hide");
   code2.classList.add("hide");
   code3.classList.add("hide");
   code4.classList.add("hide");
}