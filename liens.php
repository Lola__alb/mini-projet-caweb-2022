<main>
	<section>
		<div class="container css-definition-bloc">
			<div class="liens-image-top"></div>
			<h1>Les liens utiles</h1>
			<div class="liens-bloc">
				<p> 
					Entraidons-nous ! Vous souhaitez nous faire part de sites ou 
					de pages de documentation que vous avez trouvé utiles ? N'hésitez pas ! 
					Soumettez vos liens avec un petit commentaire pour nous expliquer 
					le sujet de la page (facultatif) :  
				</p>
				<form name="form" id="form1" method="post" action="addlink.php">
					<label>Lien :</label>
					<input type="text" name="lien" value="http://" />
					<label>Commentaire du lien :</label>
					<input type="text" name="commentaire" />
					<input type="hidden" name="css" value="<?php echo $css; ?>"/>
					<input name="submit" value="Soumettre" type="submit" />
				</form>
			</div>
			<div class="liens-submitted">
				<ul>
					<?php include "liens.html"; ?>
				</ul>
			</div>
			<div class="liens-image-bottom"></div>
		</div>
	</section>
</main>