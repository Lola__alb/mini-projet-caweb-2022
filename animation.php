<main>
	<div class="css-image"></div>
	<section>
		<div class="container css-definition-bloc">
			<h1>Les Animations</h1>
			<p>
				Une des grandes nouveautés de CSS3 (qui n'en est plus vraiment une depuis le temps) est la propriété 
				<strong><code>animation</code></strong>. Elle permet, comme son nom l'indique, de créer des animations
				entre différents styles. Il s'agit plus précisément d'une <a href="https://developer.mozilla.org/fr/docs/Web/CSS/Shorthand_properties">propriété raccourcie</a>. 
				À vous les joies de créer des animations sans avoir à faire de Javascript. 
			</p>
			<p>
				Pour faire simple, la propiété génère les étapes intermédiaires entre deux styles. Mais vous pouvez le faire 
				sur autant de propriétés CSS que vous voulez, autant de fois que vous le souhaitez. 
				Vous aurez juste à indiquer les images clés en utilisant <strong><code>@keyframes</code></strong> et toutes les 
				interpollations seront calculées automatiquement.</p>
		</div>
	</section>
	<section>
		<div class="container css-table-bloc">
			<h2>Syntaxe</h2>
			<p>La syntaxe à proprement parlé n'est pas sticte, il s'agit en fait 
				d'un ensemble de paramètres indiquant la durée, le nombre d'itérations 
				ou encore le délais de l'animation.</p>
			<div class="css-table">
				<table>
					<tr><th>propriété</th><th>Descriptions</th><th>Valeurs attendues</th></tr>
					<tr><td>@keyframes</td><td>Liste l'ensemble des images clés de votre animation</td><td>Valeur en pourcentage</td></tr>
					<tr><td>animation-name</td><td>Indique le nom de l'animation et permet de relier l'animation a l'élément animé</td><td>une chaine de caractère</td></tr>
					<tr><td>animation-duration</td><td>Indique la durée de l'animation</td><td>Valeur en seconde</td></tr>
					<tr><td>animation-delay</td><td>indique le delai avant l'execution de l'animation</td><td>Valeur en seconde</td></tr>
					<tr><td>animation-iteration-count</td><td>Indique le nombre de fois ou l'animation va se répéter</td><td>Nombre entier</td></tr>
					<tr><td>animation-direction</td><td>Indique le sens dans lequel l'animation va se dérouler</td><td>Prend les valeurs "normal", "reverse" ou "alternate"</td></tr>
					<tr><td>animation-timing-function</td><td>Défini la variation de vitesse de l'animation</td><td>"ease", "linear", cubic(n,n,n,n) etc...</td></tr>
					<tr><td>animation-fill-mode</td><td>Spécifie le style de l'élément quand l'animation ne se joue pas</td><td>"none", "normal", "forwards", "backwards" etc...</td></tr>
					<tr><td>animation</td><td>permet de preciser tous les paramètres précédents en une seul ligne</td><td>animation: example 5s linear 2s infinite alternate;</td></tr>
				</table>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<h2>Exemples</h2>
			<div class="animation-example-container">
				<div class="button-list">
					<button id="reset">réinitialiser</button>
					<button id="anime1">Faire sautiller Jason</button>
					<button id="anime2">Faire rouler Jason</button>
					<button id="anime3">Faire léviter Jason</button>
					<button id="anime4">Faire écouter de l'EDM à Jason</button>
				</div>
				<div class="display-example">
					<div class="animated-example">

					</div>
				</div>
				<div class="description code0">
					<p>Ici sera affiché le code de l'animation</p>
				</div>
				<div class="description code1 hide">
					<pre><code>
animation: bounce 0.5s ease-out infinite alternate;
transform-origin: bottom;

@keyframes bounce {
 0% {
  transform: scaleY(0.80)
 }
 10% {
  transform: translateY(0);
 }
 100% {
  transform: translateY(-150px);
 }
}</code></pre>
					</div>
					<div class="description code2 hide">
					<pre><code>
.display-example .animated-example.duckJump{
	animation:roll 3s linear infinite
}

@keyframes roll{
	0%{
		transform:rotate(0);
		left: 0%;
	}
	50%{
		left: calc(100% - 100px);
		transform:rotate(720deg);
	}
	100%{
		transform:rotate(0);
		left: 0%;
	}
}
}</code></pre>					</div>
					<div class="description code3 hide">
					<pre><code>
.display-example .animated-example.duckFloat{
	animation:rollFloat 2s linear, 
	float 2s ease 2s infinite alternate;
}

@keyframes rollFloat{
	0%{
		transform:rotate(-360deg);
		left: 0%;
	}
	70%{
		transform:rotate(0);
		left: calc(50% - 100px);
		bottom: 0px;
	}
	100%{
		left: calc(50% - 100px);
		bottom:100px;
	}
}
@keyframes float{
	0%{
		left: calc(50% - 100px);
		bottom: 100px;
	}
	100%{
		left: calc(50% - 100px);
		bottom: 130px;
	}
}
}</code></pre>						</div>
					<div class="description code4 hide">
					<pre><code>
.display-example .animated-example.duckAppear {
	animation: appear 0.5s linear infinite both;
}

@keyframes appear {
  0% {
  	left: calc(50% - 50px);
  	bottom: calc(50% - 50px);
  	filter: hue-rotate(0deg);
    transform: translate(0);
  }
  10% {
    transform: translate(-3px, -3px);
  }
  20% {
    transform: translate(3px, -3px);
  }
  30% {
    transform: translate(-3px, 3px);
  }
  40% {
    transform: translate(3px, 3px);
  }
  50% {
    transform: translate(-3px, -3px);
  }
  60% {
    transform: translate(3px, -3px);
  }
  70% {
    transform: translate(-3px, 3px);
  }
  80% {
    transform: translate(-3px, -3px);
  }
  90% {
    transform: translate(3px, -3px);
  }
  100% {
  	  	left: calc(50% - 50px);
  	bottom: calc(50% - 50px);
  	filter: hue-rotate(360deg);
    transform: translate(0);
  }
}
}</code></pre>						</div>
				</div>
			</div>
		</section>
	</main>
	<script src='js/animations.js'></script>