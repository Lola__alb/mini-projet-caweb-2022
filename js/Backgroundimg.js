/*Animations pour la page css background-image*/
function getSelector(id){
    return document.querySelector(id)
}

const bgExampleOne = getSelector('.bg-example-one')
const bgButtons = document.querySelectorAll(".bg-img")

bgButtons.forEach(function(elt){
    elt.addEventListener("click", transformElement)
})

function resetPosition(para){
    bgExampleOne.style.backgroundImage = "none"
}

function transformElement(event){
    resetPosition(event.target.classList)
    switch(event.target.id){
        case "bg-one" : 
        bgExampleOne.style.backgroundImage = "url('css/images/Lola/canardsquare.png')"
        bgExampleOne.style.backgroundRepeat = "repeat"
        break;
        case "bg-two" : 
        bgExampleOne.style.backgroundImage = "url('css/images/Lola/canardsquare.png')";
        bgExampleOne.style.backgroundRepeat = "no-repeat"
        break;
        case "bg-three" : 
        bgExampleOne.style.backgroundImage = "linear-gradient(45deg, #fbd786, #f7797d)"
        break;
        case "bg-four" : 
        bgExampleOne.style.backgroundImage = "repeating-radial-gradient(#22c1c3 70%, #fdbb2d)"
        break;
        case "bg-five" : 
        bgExampleOne.style.backgroundImage = "none"
        break;
    }
}



