<main>
	<div class="css-image"></div>
	<section>
		<div class="container css-definition-bloc">
			<h1>Background-image</h1>
			<p>
				La propriété <code>background-image</code> permet d'ajouter une <b>image</b> ou un <b>dégradé</b> de fond à un élément, sans utiliser de balise <code>img</code>.
			</p>
		</div>
	</section>
	<section>
		<div class="container css-syntaxe-bloc">
			<h2>Syntaxe</h2>
			<p><code>background-image : <i>url('')</i> |  <i>gradient-function()</i> | none | initial | inherit</code></p>
		</div>
	</section>
	<section>
		<div class="container css-table-bloc">
			<h2>Valeurs</h2>
			<p>
				L'image se positionne par défaut en haut à gauche de son conteneur et se répète autant de fois qu'il le faut pour en remplir toute la surface. Mais ces comportements sont modifiables avec 
				des propriétés comme <code>background-repeat</code> ou <code>background-size</code>.<br/>
				Le dégradé, lui, occupe toute la surface de l'élément sans se répéter. Par défaut, il va de haut en bas pour un dégradé linéaire, de l'intérieur vers 
				l'extérieur pour un dégradé radial et tourne de droite à gauche pour un dégradé conique.
			</p>
			<div class="css-table">
				<table>
					<tr>
						<th>Valeur</th>
						<th>Description</th>
					</tr>
					<tr>
						<td><code>none</code></td>
						<td>Aucune image de fond n'est appliquée sur l'élément (valeur par défaut)</td>
					</tr>
					<tr>
						<td><code>url('image-URL')</code></td>
						<td>L'image indiquée en argument (visée depuis son url) sera affichée</td>
					</tr>
					<tr>
						<td><code>linear-gradient(couleur1, couleur2, ...)</code></td>
						<td>L'élément a pour fond un dégradé linéaire composé des couleurs passées en arguments (au moins deux couleurs)</td>
					</tr>
					<tr>
						<td><code>radial-gradient(couleur1, couleur2, ...)</code></td>
						<td>L'élément a pour fond un dégradé radial composé des couleurs passées en arguments (au moins deux couleurs)</td>
					</tr>
					<tr>
						<td><code>conic-gradient(couleur1, couleur2, ...)</code></td>
						<td>L'élément a pour fond un dégradé conique composé des couleurs passées en arguments (au moins deux couleurs)</td>
					</tr>
					<tr>
						<td><code>repeating-linear-gradient()</code></td>
						<td>Créé un dégradé linéaire qui se répète plusieurs fois</td>
					</tr>
					<tr>
						<td><code>repeating-radial-gradient()</code></td>
						<td>Créé un dégradé radial qui se répète plusieurs fois</td>				</tr>
					<tr>
						<td><code>repeating-conic-gradient()</code></td>
						<td>Créé un dégradé conique qui se répète plusieurs fois</td>				</tr>
					<tr>
						<td><code>initial</code></td>
						<td>La propriété background-image est définie sur sa valeur par défaut</td>
					</tr>
					<tr>
						<td><code>inherit</code></td>
						<td>La propriété background-image prend les mêmes valeurs que le parent de l'élément ciblé</td>
					</tr>			
				</table>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div>
				<h2>Exemples</h2>
				<p>
					La propriété est simple d'utilisation, vous verrez que cela ne casse pas trois pattes à un canard. 
					Testez ici les effets que peuvent produire chacune de ces valeurs associées à la propriété <code>background-image</code> :
				</p>
			</div>
			<div class="example bg-img-example">
				<div class="example-list">
					<div class="example-code">
						<div>
							<code>background-image : url('image.png')</code>
							<button class="bg-img example-list-one" id="bg-one">Tester</button>
						</div>
						<div>
							<code>background-repeat : no-repeat</code>
							<button class="bg-img example-list-one" id="bg-two">Tester</button>
						</div>
						<div>
							<code>background-image : linear-gradient(45deg, #fbd786, #f7797d)</code>
							<button class="bg-img example-list-one" id="bg-three">Tester</button>
						</div>
						<div>
							<code>background-image : repeating-radial-gradient(#22c1c3 70%, #fdbb2d)
							</code>
							<button  class="bg-img example-list-one" id="bg-four">Tester</button>
						</div>
						<div>
							<code>background-image : none</code>
							<button  class="bg-img example-list-one" id="bg-five">Reset</button>
						</div>
					</div>
				</div>
				<div class="display-example">
					<div class="bg-example bg-example-one"></div>
				</div>
			</div>
		</div>
	</section>
</main>
<script src="js/Backgroundimg.js"></script>