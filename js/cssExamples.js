/*Animations pour la page css transform*/
function getSelector(id){
    return document.querySelector(id)
}

const transformExampleOne = getSelector('.transform-example-one')
const transformExampleTwo = getSelector('.transform-example-two')

const transformButtons = document.querySelectorAll(".transform")

transformButtons.forEach(function(elt){
    elt.addEventListener("click", transformElement)
})

function resetPosition(para){
    if(para.contains("example-list-one") ){
    transformExampleOne.style.transform = "translate(-40px,-40px)"
    transformExampleOne.style.transform = "rotate(-90deg)"
    transformExampleOne.style.transform = "scale(1)"
    transformExampleOne.style.transform = "skew(-10deg)"
    }
    else{
    transformExampleTwo.style.transform = "translate3D(-10px, -50%, -1em) skewY(-30deg)"
    transformExampleTwo.style.transform = "rotate(-180deg) scale(1) translateX(-30px)"
    transformExampleTwo.style.transform = "matrix(-1, -2, -2, -1, -3, -1)"
    }
}

function transformElement(event){
    resetPosition(event.target.classList)
    switch(event.target.id){
        case "transform-one" : 
        transformExampleOne.style.transform = "translate(40px,40px)"
        break;
        case "transform-two" : 
        transformExampleOne.style.transform = "rotate(90deg)"
        break;
        case "transform-three" : 
        transformExampleOne.style.transform = "scale(2)"
        break;
        case "transform-four" : 
        transformExampleOne.style.transform = "skew(25deg)"
        break;
        case "transform-five" : 
        transformExampleOne.style.transform = "none"
        break;
        case "transform-six" : 
        transformExampleTwo.style.transform = "translate3D(10px, 50%, 1em) skewY(30deg)"
        break;
        case "transform-seven" : 
        transformExampleTwo.style.transform = "rotate(180deg) scale(.5) translateX(30px)"
        break;
        case "transform-eight" : 
        transformExampleTwo.style.transform = "matrix(1, 2, 2, 1, 3, 1)"
        break;
        case "transform-nine" : 
        transformExampleTwo.style.transform = "none"
        break;

    }

}



