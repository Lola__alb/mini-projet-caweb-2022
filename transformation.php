<main>
	<div class="css-image"></div>
	<section>
		<div class="container css-definition-bloc">
			<h1>Transformation</h1>
			<p>
				La propriété CSS <strong><code>transform</code></strong> applique une transformation 2D
				ou 3D à un élément. Elle permet de le redimensionner, le déplacer sur un axe X ou Y, 
				le distordre, changer son échelle, ou encore le faire pivoter.
			</p>
		</div>
	</section>
	<section>
		<div class="container css-syntaxe-bloc">
			<h2>Syntaxe</h2>
			<p><code>transform : <i>transform-function()</i> | none | initial | inherit</code></p>
		</div>
	</section>
	<section>
		<div class="container css-table-bloc">
			<h2>Valeurs (transform-functions)</h2>
			<p>
				<code>transform </code> peut prendre plusieurs valeurs parmis les propriétés suivantes. 
				Celles qui acceptent des valeurs en argument, à l'exception de <code>matrix()</code>,
				peuvent toutes s'écrire avec la syntaxe <code>propriétéX(x)</code>, <code>propriétéY(y)</code>ou <code>propriétéZ(z)</code>
				si ne souhaite que manipuler le comportement de l'élément que sur un axe précis.
				Par exemple, <code>translate(x,y)</code> peut s'écrire <code>translateX(x)</code> si on veut changer la position de l'élément 
				sur l'axe des abscisses. Ceci est valable pour <code>translate, scale, skew</code> et <code>rotate</code>.
			</p>
			<div class="css-table">
				<table>
					<tr>
						<th>Valeur</th>
						<th>Description</th>
					</tr>
					<tr>
						<td><code>none</code></td>
						<td>Aucun effet de transformation n'est appliqué (valeur par défaut)</td>
					</tr>
					<tr>
						<td><code>translate(x,y)</code></td>
						<td>L'élément se déplace de tant et tant sur les axes x et y</td>
					</tr>
					<tr>
						<td><code>translate3d(x,y,z)</code></td>
						<td>L'élément se déplace de tant et tant sur trois dimensions</td>
					</tr>
					<tr>
						<td><code>scale(x,y)</code></td>
						<td>L'élément se redimmensionne en hauteur et largeur selon les valeurs données</td>
					</tr>
					<tr>
						<td><code>scale3d(x,y,z)</code></td>
						<td>L'élément se redimmensionne sur trois dimensions selon les valeurs données</td>
					</tr>
					<tr>
						<td><code>rotate(angle)</code></td>
						<td>L'élément fait une rotation selon l'angle donné</td>
					</tr>
					<tr>
						<td><code>rotate3d(x,y,z,angle)</code></td>
						<td>L'élément fait une rotation sur trois dimensions selon l'angle donné</td>
					</tr>
					<tr>
						<td><code>skew(angle x, angle y)</code></td>
						<td>L'élément se penche selon les angles donnés</td>
					</tr>
					<tr>
						<td><code>matrix (scaleX(), skewY(), skewX(), scaleY(), translateX(), translateY())</code></td>
						<td>L'élément prend un effet de transformation regroupant plusieurs valeurs</td>
					</tr>
					<tr>
						<td><code>matrix3d (16x n)</code></td>
						<td>Matrice en 4x4 de 16 valeurs</td>
					</tr>
					<tr>
						<td><code>initial</code></td>
						<td>La propriété transform est définie sur sa valeur par défaut</td>
					</tr>
					<tr>
						<td><code>inherit</code></td>
						<td>La propriété transform prend les mêmes valeurs que le parent de l'élément ciblé</td>
					</tr>			
				</table>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<div>
				<h2>Exemples</h2>
				<p>
					Testez ici les effets que peuvent produire chacune de ces valeurs associées à la propriété <code>transform</code> :
				</p>
			</div>
			<div class="example">
				<div class="example-list">
					<div class="example-code">
						<div>
							<code>transform : translate(40px, 40px)</code>
							<button class="transform example-list-one" id="transform-one">Tester</button>
						</div>
						<div>
							<code>transform : rotate(90deg)</code>
							<button class="transform example-list-one" id="transform-two">Tester</button>
						</div>
						<div>
							<code>transform : scale(2)</code>
							<button class="transform example-list-one" id="transform-three">Tester</button>
						</div>
						<div>
							<code>transform : skew(25deg)</code>
							<button  class="transform example-list-one" id="transform-four">Tester</button>
						</div>
						<div>
							<code>transform : none</code>
							<button class="transform example-list-one" id="transform-five">Reset</button>
						</div>
					</div>
				</div>
				<div class="display-example">
					<div class="transform-example transform-example-one"></div>
				</div>
			</div>

			<div>
				<h3>Exemples de combinaisons</h3>
				<p>
					Bien entendu, il est possible d'utiliser plusieurs valeurs en même temps, pour le même élément. On parle alors de transformation
					composée. 
					Les exemples suivants présentent ces combinaisons, une plume plus complexe que ce que nous avons vu juste avant :
				</p>
			</div>
			<div class="example">
				<div class="example-list">
					<div class="example-code">
						<div>
							<code>transform : translate3D(10px, 50%, 1em) skewY(30deg)</code>
							<button class="transform example-list-two" id="transform-six">Tester</button>
						</div>
						<div>
							<code>transform : rotate(180deg) scale(.5) translateX(30px)</code>
							<button class="transform example-list-two" id="transform-seven">Tester</button>
						</div>
						<div>
							<code>transform : matrix(1, 2, 2, 1, 3, 1);</code>
							<button class="transform example-list-two" id="transform-eight">Tester</button>
						</div>
						<div>
							<code>transform : none</code>
							<button class="transform example-list-two" id="transform-nine">Reset</button>
						</div>
					</div>
				</div>
				<div class="display-example">
					<div class="transform-example transform-example-two"></div>
				</div>
			</div>
		</div>
	</section>
</main>
<script src="js/cssExamples.js"></script>