/*Bruit déclenché au clic sur le logo dans le header*/

const coin = new Audio('js/duck.mp3')
coin.preload = 'auto';

document.querySelector('.logo-item').addEventListener("click", duck)


function duck(){
    coin.play();
    var url_string = window.location.href; 
    var url = new URL(url_string);
    var css = url.searchParams.get("css");
    setTimeout(() => {  document.location.href="index.php?page=home&css="+css+"&titre=introduction";}, 300);
    
}

