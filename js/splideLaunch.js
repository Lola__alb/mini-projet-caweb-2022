document.addEventListener( 'DOMContentLoaded', function () {
  new Splide( '#carousel-Pierre', {
    type: 'loop',
    fixedHeight: '200px',
  } ).mount();

  new Splide('#carousel-Lola', {
    type: 'loop',
    // heightRatio:0.3,
    fixedHeight: '400px'
  } ).mount();

} );

