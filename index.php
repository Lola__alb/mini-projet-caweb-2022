<?php

$allowed_page=['transformation','animation','background', 'liens'];
$allowed_css=['css0','css1','css2'];
$page='';
$css='';
$titre='';

if(isset($_REQUEST["page"])){
	$page=$_REQUEST["page"];
}
if(isset($_REQUEST["css"])){
	$css=$_REQUEST["css"];
}
if(isset($_REQUEST["titre"])){
	$titre=$_REQUEST["titre"];
}
if (($page=='')||!in_array($page, $allowed_page))
{
	$page = "home";
	$titre = "introduction";
}
if (($css=='')||!in_array($css, $allowed_css))
{
		$css = "css1";
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<title><?php echo "Site dynamique : ".$titre ?></title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo '<link href="css/'.$css.'.css"'. ' rel="stylesheet" type="text/css" media="screen">'; ?>
	<link rel="stylesheet" href="css/splide.min.css">
	<link rel="stylesheet" href="css/css3.css" type="text/css" media="print">
</head>
<body>
	<?php
// inclusion des fichiers
include "haut.php";//haut de page et menu
echo "\n";
include $page.".php"; //page à inclure en fonction de la valeur de $page
echo "\n";
include "bas.php"; // bas de page
?>
</html>
<script src="js/splide.min.js"></script>
<script src="js/splideLaunch.js"></script>
<?php 
if ($css == "css1") {
	echo "<script src='js/Pierre/perso.js'></script>";
}else{
	echo "<script src='js/Lola/persoLola.js'></script>";
}
?>
