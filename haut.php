<header>
	<div class="banner" id="banner"><p>Le site qui fera de vous un vrai QuaCss !</p></div>
		<nav>
			<a href="#" class="toggle-button" id="menu">
				<span class="bar"></span>
				<span class="bar"></span>
				<span class="bar"></span>
			</a>
			<ul>
				<li class="logo-item"><?php echo "<a href='index.php?page=home&css=".$css."&titre=introduction'><div class='logo'></div></a>"; ?></li>
				<li class="item ">
					<?php echo "<a href='index.php?page=home&css=".$css."&titre=introduction'>Accueil</a>"; ?>
				</li>
				<li class="sub-menu item ">
					<a href="#">CSS3</a>
					<ul class="dropdown-menu">
						<li>
							<?php echo "<a href='index.php?page=transformation&css=".$css."&titre=Transformations'>Transformation</a>"; ?>
						</li>
						<li>
							<?php echo "<a href='index.php?page=animation&css=".$css."&titre=Animations'>Animation</a>"; ?>
						</li>
						<li>
							<?php echo "<a href='index.php?page=background&css=".$css."&titre=Background'>Background-image</a>"; ?>

						</li>
					</ul>
				</li>
				<li class="item">
					<?php echo "<a href='index.php?page=liens&css=".$css."&titre=Liens'>Les Liens utiles</a>"; ?>
				</li>
			</ul>
	</nav>


</header>
<script src="js/toggle_button.js"></script>
<script src="js/coin.js"></script>