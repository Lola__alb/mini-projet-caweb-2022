nav = document.querySelector("nav")
logo = document.querySelector("logo-item")
topPosition = nav.getBoundingClientRect().top
currentScrollPosition =  window.pageYOffset

//Header : cacher la banière pour les autres pages que la page d'accueil
window.onload = function() {
	if(window.location.href.indexOf("introduction") > -1){
		document.getElementById("banner").style.display = "block";

		if (currentScrollPosition == topPosition){
			nav.classList.add('nav_home')
		}
	}
}

//Header : nav qui change de couleur au scroll
window.onscroll = function() {
	currentScrollPosition =  window.pageYOffset
		if (window.location.href.indexOf("introduction") > -1 && currentScrollPosition > 0){
			nav.classList.remove('nav_home')

		}
		else if(window.location.href.indexOf("introduction") > -1 && currentScrollPosition == 0){
			nav.classList.add('nav_home')

		}
}
