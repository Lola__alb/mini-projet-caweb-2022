<main>
	<section class="intro">
		<div class="container">
			<div>
				<h1>Bienvenue sur QuaCss !</h1>
				<p>
					Le but de notre petit site est de vous présenter plusieurs manières de  
					rendre vos pages web plus attrayantes à l'aide de fonctionnalités CSS. 
					Nous avons réalisé ce site pour nous entraîner nous-mêmes et mettre en pratique 
					ce que nous avons appris. Nous ne serons donc pas exhaustifs. 
				</p>
				<p>
					Nous avons choisi de vous montrer comment faire des animations, intégrer 
					des images et dégradés de fond, et utiliser la propriété transform.
					Nous espérons de tout coeur que cela vous plaira ! Ou au moins que vous aimez les canards.
				</p>
			</div>
			<div class="home-main-image"></div>
		</div>
	</section>
	<div class="home-separation-image"></div>
	<section class="slider">
		<div class="container">
			<h2>Qui sommes-nous ?</h2>
			<p>
				Nous sommes un petit collectif de deux étudiants avec un fort intérêt 
				pour le développement web et la création de sites. Notre collectif compte également
				un troisième membre, qui éprouve plus d'intérêt pour les croûtes de pain que pour le web : 
				il s'agit de notre mascotte, Jason le canard. Voilà déjà plusieurs mois que cette petite 
				boule blanche squatte notre bureau et donne de la vie à ce lieu. Nous avons donc voulu 
				honorer Jason en dédiant ce site aux canards. Nous espérons que ça lui fera plaisir, même 
				si c'est un canard et qu'il ne sait pas utiliser un ordinateur.
			</p>

			<!--slider pierre-->
			<section id="carousel-Pierre" class="splide splide-Pierre" aria-label="Slide Container Example">
				<div class="splide__track">
					<ul class="splide__list">
						<li class="splide__slide">
							<div class="splide__slide__container">
								<img src="css/images/Pierre/Lola.png" alt="">
							</div>
							<div class="splide-legend">
								<p>Lola</p>
								<p>Développeuse front-end et graphiste qui a un amour non dissimulé pour les canards. Ici illustré par une image générée par Stable Diffusion. Parce que pourquoi pas.</p>
							</div>
						</li>
						<li class="splide__slide">
							<div class="splide__slide__container">
								<img src="css/images/Pierre/Pierre.png" alt="">
							</div>
							<div class="splide-legend">
								<p>Pierre</p>
								<p>Développeur full-stack, allergique aux plumes mais rempli d'amour pour les canards néanmoins. Toujours illustré par stable diffusion peut être par timidité.</p>
							</div>
						</li>
						<li class="splide__slide">
							<div class="splide__slide__container">
								<img src="css/images/Pierre/Jason.png" alt="">
							</div>
							<div class="splide-legend">
								<p>Jason</p>
								<p>"Coin coin coincoin coin coin. Coincoin coin coin coin coin coinc ocinc ocinc ocinc ocin coicn cocinc."</p>
							</div>
						</li>
					</ul>
				</div>
			</section>

				<!--slider Lola-->
		<section id="carousel-Lola" class="splide splide-Lola" aria-label="Slide Container Example">
			<div class="splide__track">
				<ul class="splide__list">
					<li class="splide__slide">
						<div class="splide__slide__container">
							<img src="css/images/Lola/Mascotte.png" alt="portrait dessiné de Jason le canard">
						</div>	
						<div class="splide-Lola-legend">
							<p>Jason</p>
							<p>Notre mascotte ! A déjà réussi à coder par hasard un script JS en marchant sur un clavier</p>
						</div>	
					</li>
					<li class="splide__slide">
						<div class="splide__slide__container">
							<img src="css/images/Lola/Pierre.png" alt="portrait dessiné de Pierre">
						</div>
						<div class="splide-Lola-legend">
							<p>Pierre</p>
							<p>Développeur full-stack, Pierre est toujours à l'affut des dernières évolutions technologiques </p>
						</div>	
					</li>
					<li class="splide__slide">
						<div class="splide__slide__container">
							<img src="css/images/Lola/Lola.png" alt="portrait dessiné de Lola">
						</div>
						<div class="splide-Lola-legend">
							<p>Lola</p>
							<p>Développeuse front-end et graphiste très emballée à l'idée d'illustrer pleins de canards </p>					
						</div>	
					</li>
				</ul>
			</div>
		</section>
	</section>
</main>
<script src="js/Lola/persoLola.js"></script>