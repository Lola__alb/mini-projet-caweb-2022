# Mini projet CAWEB 2022
Ce site est un devoir pour le cours de développement front-end du master CAWEB. Nous travaillons dessus en binôme, avec chacun sa fiche CSS, comme demandé dans les consignes. L'utilisateur peut changer la CSS depuis le footer.

La consigne était de créer le site d'un jeune collectif prit d'intérêt pour l'informatique, qui souhaite présenter les changements qu'ont apporté CSS3.

Il nous a également été demandé de créer un logo, une bannière et un diaporama.
