/*Script déclenché au clic sur le bouton "jason", qui fait sortir un petit canard du coin de la page*/
document.querySelector("#call-Jason").addEventListener("click", callJason);

const jason = document.querySelector(".jason");

function callJason(){
    if(jason.style.right=="-100px"){
        jason.style.right="-194px";
        jason.style.transform = "rotate(45deg)";
    }
    else{
        jason.style.right="-100px";
        jason.style.transform = "rotate(-45deg)";
    }
}

